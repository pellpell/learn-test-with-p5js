'use strict';

// Import the expect library.  This is what allows us to check our code.
// You can check out the full documentation at http://chaijs.com/api/bdd/
import { expect } from "chai";
import { describe, it } from "mocha";

// Import our ColorIncreaser class.
import { ColorIncreaser } from "../src/color_increaser";

import { FillColor } from "../src/fill_color";

describe('ColorIncreaser tests', () => {
    // Will hold the reference to the ColorIncreaser classF
    let colorIncreaser: ColorIncreaser;

    // beforeEach is a special function that is similar to the setup function in
    // p5.js.  The major difference it that this function runs before each it()
    // test you create instead of running just once before the draw loop
    // beforeEach lets you setup the objects you want to test in an easy fashion.
    beforeEach(() => {
        let colorValueIncrease = 1;
        let fillColor = new FillColor(0, 0, 0, 255);
        colorIncreaser = new ColorIncreaser(colorValueIncrease, fillColor);
    });

    it('should be an object', (done) => {
        expect(colorIncreaser).to.be.a('object');
        done();
    });

    it('should store initial values without mutation', (done) => {
        expect(colorIncreaser.colorValueIncrease).to.be.equal(1);
        expect(colorIncreaser.fillColor.red).to.equal(0);
        expect(colorIncreaser.fillColor.green).to.equal(0);
        expect(colorIncreaser.fillColor.blue).to.equal(0);
        expect(colorIncreaser.fillColor.alpha).to.equal(255);
        done();
    });

    it('should have rgb values 255, 0, 0 after calling increaseFillColor 255 times', (done) => {
        //it is 256^1 - 1 because it starts with the color black
        for (let count = 0; count < 255; count += 1) {
            colorIncreaser = colorIncreaser.increaseFillColor();
        }
        expect(colorIncreaser.fillColor.red).to.equal(255);
        expect(colorIncreaser.fillColor.green).to.equal(0);
        expect(colorIncreaser.fillColor.blue).to.equal(0);
        done();
    });


    it('should have rgb values 255, 255, 0 after calling increaseFillColor 65535 times', (done) => {
        //it is 256^2 - 1 because it starts with the color black
        for (let count = 0; count < 65535; count += 1) {
            colorIncreaser = colorIncreaser.increaseFillColor();
        }
        expect(colorIncreaser.fillColor.red).to.equal(255);
        expect(colorIncreaser.fillColor.green).to.equal(255);
        expect(colorIncreaser.fillColor.blue).to.equal(0);
        done();
    });


    it('should have rgb values 255, 255, 255 after calling increaseFillColor 16777215 times', (done) => {
        //it is 256^3 - 1 because it starts with the color black
        for (let count = 0; count < 16777215; count += 1) {
            colorIncreaser = colorIncreaser.increaseFillColor();
        }
        expect(colorIncreaser.fillColor.red).to.equal(255);
        expect(colorIncreaser.fillColor.green).to.equal(255);
        expect(colorIncreaser.fillColor.blue).to.equal(255);
        done();
    });

    it('should have rgb values 0, 0, 0 after calling increaseFillColor 16777216 times', (done) => {
        //it is 256^3 because it starts with the color black
        for (let count = 0; count < 16777216; count += 1) {
            colorIncreaser = colorIncreaser.increaseFillColor();
        }
        expect(colorIncreaser.fillColor.red).to.equal(0);
        expect(colorIncreaser.fillColor.green).to.equal(0);
        expect(colorIncreaser.fillColor.blue).to.equal(0);
        done();
    });
});
