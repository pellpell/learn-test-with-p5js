'use strict';

// Import the expect library.  This is what allows us to check our code.
// You can check out the full documentation at http://chaijs.com/api/bdd/
import { expect } from "chai";
import { describe, it } from "mocha";

import { FillColor } from "../src/fill_color";

describe('FillColor tests', () => {
    let fillColor: FillColor;

    beforeEach(() => {
        fillColor = new FillColor(0, 0, 0, 255);
    });

    it('should be an object', (done) => {
        expect(fillColor).to.be.a('object');
        done();
    });

    it('should store initial values without mutation', (done) => {
        expect(fillColor.red).to.equal(0);
        expect(fillColor.blue).to.equal(0);
        expect(fillColor.green).to.equal(0);
        expect(fillColor.alpha).to.equal(255);
        done();
    });
});
