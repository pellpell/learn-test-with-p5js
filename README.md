# Learn test with p5.js

Learn Unit Test and Test Driven Development (TDD) with p5.js on TypeScript.

Reference: [https://p5js.org/learn/tdd.html](https://p5js.org/learn/tdd.html)

Notion page: [Learn-test-with-p5-js in Notion (主に日本語)](https://pellpell.notion.site/Learn-test-with-p5-js-1be1ea462e624cf9932ff5bf390ca409)

## Setup

install node and yarn (or npm)

This project:

- node: v16.17.0
- yarn: 1.22.19

1. create a new project folder
    
    ```bash
    mkdir learn-test-with-p5js
    ```
    
    ```bash
    cd learn-test-with-p5js
    ```
    
2. create `package.json` as
    
    ```json
    {
        "name": "learn-test-with-p5js",
        "version": "0.1.0",
        "private": true
    }
    
    ```
    
3. in terminal, run
    
    ```bash
    yarn add p5
    ```
    
    ```bash
    yarn add -D @types/p5 parcel
    ```
    
    Then, added “dependencies” and “devDependencies” in `package.json` .
    
4. add scripts in `package.json` 
    
    ```json
    {
        "name": "learn-test-with-p5js",
        "version": "0.1.0",
        "private": true,
        "scripts": {
            "dev": "parcel src/index.html --open",
            "build": "parcel build src/index.html --public-url . --dist-dir=docs"
        },
        "dependencies": {
            "p5": "^1.4.2"
        },
        "devDependencies": {
            "@types/p5": "^1.4.2",
            "parcel": "^2.7.0"
        }
    }
    ```
    
5. mkdir `src` and add `src/index.html` as
    
    ```html
    <!DOCTYPE html>
    <html lang="ja">
    
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Learn test with p5.js</title>
        <script src="main.ts" type="module"></script>
        <link rel="stylesheet" href="index.css">
    </head>
    
    <body>
    </body>
    
    </html>
    ```
    
6. add `src/main.ts` as
    
    ```tsx
    import p5 from "p5";
    
    const sketch = (p: p5) => {
    
        /** 初期化処理 */
        p.setup = () => {
            p.createCanvas(400, 400);
        };
    
        /** 描画処理 */
        p.draw = () => {
            p.background(220);
            p.ellipse(50,50,80,80);
        };
    };
    
    new p5(sketch);
    ```
    
7. add `src/index.css` as
    
    ```css
    body {
        padding: 0;
        margin: 0;
    }
    ```
    
8. install packages
    
    ```bash
    yarn install
    ```
    
9. start
    
    ```bash
    yarn dev
    ```
    
    <aside>
    ⚠️ if you use Windows and WSL:
    
    if you error as :
    
    ```bash
    
    ...
    
    node:events:491
          throw er; // Unhandled 'error' event
          ^
    
    Error: spawn cmd.exe ENOENT
    
    ...
    
    ```
    
    then, you add `cmd.exe` PATH
    
    ```bash
    export PATH=$PATH:/mnt/c/Windows/System32
    ```
    
    and run again `yarn dev` !
    
    </aside>
    
10. (opitional) if you are using git:
    
    add `.gitignore` 
    
    ```
    node_modules
    .parcel-cache
    
    dist
    
    .DS_Store
    ```

Then, let's read and learn test in [learn | p5.js (p5js.org)](https://p5js.org/learn/tdd.html) !
