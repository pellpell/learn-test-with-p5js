import p5 from "p5";

export class FillColor {
    constructor(
        public readonly red: number,
        public readonly green: number,
        public readonly blue: number,
        public readonly alpha: number) {
    }

    color(p: p5): p5.Color {
        return p.color(this.red, this.green, this.blue, this.alpha);
    }
} 
