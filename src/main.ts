// This is for a unit test tutorial.
// it should create a rectangle and allow you to iterate over
// every single color.
//
// colorValueIncrease sets the amount the color changes on each
// draw loop. Values greater than 255 will break the sketch.
// fillColor will be the color of the rectangle.
// colorIncreaser will become an instance of our ColorIncreaser class.

import p5 from "p5";

import { ColorIncreaser } from "./color_increaser";
import { FillColor } from "./fill_color";

const sketch = (p: p5) => {

    let colorIncreaser: ColorIncreaser;

    /** 初期化処理 */
    p.setup = () => {
        p.createCanvas(500, 500);
        p.background(0);
        p.noStroke();
        let colorValueIncrease = 1;
        let fillColor = new FillColor(0, 0, 0, 255);
        colorIncreaser = new ColorIncreaser(colorValueIncrease, fillColor);
    };

    /** 描画処理 */
    p.draw = () => {
        let color = colorIncreaser.fillColor.color(p);

        p.fill(color);
        p.rect(0, 0, 500, 500);

        // update colorIncreaser
        colorIncreaser = colorIncreaser.increaseFillColor();
    };
};

new p5(sketch);
