import { FillColor } from "./fill_color"

export class ColorIncreaser {
    constructor(
        public readonly colorValueIncrease: number,
        public readonly fillColor: FillColor) {
        // Stores a value and a color and allows you to increase the color
        // by that value.
    }

    increaseFillColor(): ColorIncreaser {
        // increase the first color channel by one.  If that channel
        // is now > 255 then increment the next color channel.  Repeat for second
        // and third channel

        let red = this.fillColor.red;
        let green = this.fillColor.green;
        let blue = this.fillColor.blue;

        red += this.colorValueIncrease

        if (red > 255) {
            red = 0
            green += this.colorValueIncrease
        }
        if (green > 255) {
            green = 0
            blue += this.colorValueIncrease
        }
        if (blue > 255) {
            blue = 0;
        }

        let fillColor = new FillColor(red, blue, green, this.fillColor.alpha);
        return new ColorIncreaser(this.colorValueIncrease, fillColor);
    }
}
